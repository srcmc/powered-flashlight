/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.config;

public interface ICommonConfig {
    /**
     * The amount of energy drained by an active flashlight per tick. If set to 0 the
     * energy requirement for the flashlight is disabled completely and other values
     * for energy related settings don't matter.
     * Range [0, inf], Default: 1
     */
    public default int energyDrainPerTick() { return 1; }

    /**
     * The maximum rate at which a flashlight is charged per tick. Might be limited by
     * the maximum charge rate of the charging device.
     * Range [0, inf], Default: 128
     */
    public default int energyChargePerTick() { return 128; }

    /**
     * The energy capacity of a flashlight.
     * Range [1, inf], Default: 10000
     */
    public default int energyCapacity() { return 10000; }

    /**
     * Minimum light level of light blocks placed by lightrays. The further a light
     * block is placed from its source the lower its light level.
     * Range [1, maxLightLevel], Default: 6
     */
    public default int minLightLevel() { return 6; }

    /**
     * Maximum light level of light blocks placed by lightrays.
     * Range [minLightLevel, 15], Default: 15
     */
    public default int maxLightLevel() { return 15; }

    /**
     * Number of ticks a light block stays in the world before it removes itself.
     * Range: [0, inf), Default: 2.
     */
    public default int lightTimeToLive() { return 2; }

    /**
     * Max block distance of lightrays.
     * Range [0, inf), Default: 20
     */
    public default int maxLightDistance() { return 20; }

    /**
     * Spacing inbetween light blocks placed by lightrays.
     * Range [0, inf), Default: 3
     */
    public default int lightSpacing() { return 3; }

    /**
     * Factor to specify the angle of outgoing lightrays (up to 90°).
     * Range: [0, 1], Default: 1/8
     */
    public default double rayAngle() { return 1/8F; }
}
