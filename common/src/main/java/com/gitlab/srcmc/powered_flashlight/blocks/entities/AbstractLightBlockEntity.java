/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.blocks.entities;

import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class AbstractLightBlockEntity extends BlockEntity {
	private final int timeToLive;
	private int age = 0;

	public AbstractLightBlockEntity(BlockEntityType<? extends AbstractLightBlockEntity> be, BlockPos blockPos, BlockState blockState) {
		super(be, blockPos, blockState);
		this.timeToLive = PoweredFlashlight.commonConfig.lightTimeToLive();
	}

	public static <T extends BlockEntity> void tick(Level level, BlockPos pos, BlockState state, T be) {
		if(!level.isClientSide()) {
			var te = (AbstractLightBlockEntity)be;

			if(++te.age > te.timeToLive) {
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 22);
			}
		}
	}
}
