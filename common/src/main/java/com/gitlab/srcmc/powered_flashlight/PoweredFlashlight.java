/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.srcmc.powered_flashlight.config.IClientConfig;
import com.gitlab.srcmc.powered_flashlight.config.ICommonConfig;
import com.gitlab.srcmc.powered_flashlight.config.IServerConfig;

public class PoweredFlashlight {
    public static final String MODID = "powered_flashlight";
    public static final String MOD_NAME = "Powered Flashlight";
	public static final Logger LOG = LoggerFactory.getLogger(MOD_NAME);

    public static ICommonConfig commonConfig = new ICommonConfig() {};
    public static IClientConfig clientConfig = new IClientConfig() {};
    public static IServerConfig serverConfig = new IServerConfig() {};
}
