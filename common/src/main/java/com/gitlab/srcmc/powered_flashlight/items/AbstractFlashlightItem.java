/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.items;

import java.util.List;

import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;

import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

public abstract class AbstractFlashlightItem extends Item {
    static class Properties extends Item.Properties {
        public Properties() {
            tab(CreativeModeTab.TAB_TOOLS);
            stacksTo(1);
        }
    }

    private final Block[] lightBlocks;
    protected SoundEvent toggleOnSound = SoundEvents.COMPARATOR_CLICK;
    protected SoundEvent toggleOffSound = SoundEvents.COMPARATOR_CLICK;
    protected SoundEvent toggleFailSound = SoundEvents.COMPARATOR_CLICK;

    public AbstractFlashlightItem(Block[] ligtBlocks) {
        super(new Properties());
        this.lightBlocks = ligtBlocks;
    }

    public AbstractFlashlightItem(CreativeModeTab creativeModeTab, Block[] ligtBlocks) {
        super(new Properties().tab(creativeModeTab));
        this.lightBlocks = ligtBlocks;
    }

    private void updateLight(Level level, Entity entity) {
        var maxLightDistance = PoweredFlashlight.commonConfig.maxLightDistance();
        var maxLightLevel = PoweredFlashlight.commonConfig.maxLightLevel();
        var minLightLevel = PoweredFlashlight.commonConfig.minLightLevel();
        var lightSpacing = PoweredFlashlight.commonConfig.lightSpacing();
        var rayAngle = PoweredFlashlight.commonConfig.rayAngle();

        HitResult hit = entity.pick(maxLightDistance, 0, false);
        var maxDistance = (int)Math.min(maxLightDistance, hit.getLocation().distanceTo(entity.getEyePosition()));
        var lightDiff = maxLightLevel - minLightLevel;
        var eyePos = entity.getEyePosition();
        var forward = entity.getForward();
        var up = entity.getUpVector(1);
        var right = forward.cross(up);
        var rays = new Vec3[] {
            forward.add(forward.vectorTo(up).multiply(rayAngle, rayAngle, rayAngle)),
            forward.add(forward.vectorTo(up.reverse()).multiply(rayAngle, rayAngle, rayAngle)),
            forward.add(forward.vectorTo(right).multiply(rayAngle, rayAngle, rayAngle)),
            forward.add(forward.vectorTo(right.reverse()).multiply(rayAngle, rayAngle, rayAngle))
        };

        var eyeBlockPos = new BlockPos(eyePos);
        var ll = minLightLevel + (maxLightLevel - minLightLevel)/2;
        placeLightAround(level, eyeBlockPos, ll);
        placeLightAround(level, eyeBlockPos.below(), ll);

        if(lightSpacing > 0) {
            for(var ray : rays) {
                for(int i = 1; i < maxDistance; i += lightSpacing) {
                    placeLightBlock(level,
                        new BlockPos(eyePos.add(ray.multiply(i, i, i))),
                        minLightLevel + (int)(lightDiff*(maxDistance - i)/(double)maxDistance));

                    if(i < maxDistance - 1 && i + lightSpacing >= maxDistance) {
                        i = maxDistance - lightSpacing - 1;
                    }
                }
            }
        }
    }

    public abstract void extractEnergy(ItemStack stack, int value);
    public abstract int getEnergy(ItemStack stack);
    public abstract int getCapacity(ItemStack stack);

    public void setActive(ItemStack stack, boolean value) {
        stack.getOrCreateTag().putBoolean("active", value);
    }

    public boolean isActive(ItemStack stack) {
        return stack.getOrCreateTag().getBoolean("active");
    }

    public boolean updateActive(ItemStack stack, Level level, Entity entity) {
        if(isActive(stack)) {
            if(isUsable(stack, entity)) {
                return true;
            }

            setActive(stack, false);
            playToggleOffSound(entity);
        }

        return false;
    }

    public boolean isPowered(ItemStack stack) {
        return getEnergy(stack) > 0 || PoweredFlashlight.commonConfig.energyDrainPerTick() <= 0;
    }

    public boolean isUsable(ItemStack stack, Entity entity) {
        return isPowered(stack)
            && !entity.isEyeInFluid(FluidTags.WATER)
            && !entity.isEyeInFluid(FluidTags.LAVA)
            && !entity.isSwimming();
    }

    @Override
    public void inventoryTick(ItemStack stack, Level level, Entity entity, int i, boolean bl) {
        if(!level.isClientSide) {
            if(updateActive(stack, level, entity)) {
                for(var stackInHand : entity.getHandSlots()) {
                    if(stackInHand == stack) {
                        updateLight(level, entity);
                        break;
                    }
                }

                extractEnergy(stack, PoweredFlashlight.commonConfig.energyDrainPerTick());
            }
        }
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand interactionHand) {
        if(!level.isClientSide) {
            ItemStack stack = player.getItemInHand(interactionHand);
            
            if(isActive(stack)) {
                setActive(stack, false);
                playToggleOffSound(player);
            } else if(isUsable(stack, player)) {
                setActive(stack, true);
                playToggleOnSound(player);
            } else {
                playToggleFailSound(player);
                return InteractionResultHolder.fail(stack);
            }

            return InteractionResultHolder.consume(stack);
        }

        return super.use(level, player, interactionHand);
    }

    public void playToggleOnSound(Entity entity) {
        entity.getLevel().playSound(null, entity.blockPosition(), toggleOnSound, SoundSource.NEUTRAL, 1, 1);
    }

    public void playToggleOffSound(Entity entity) {
        entity.getLevel().playSound(null, entity.blockPosition(), toggleOffSound, SoundSource.NEUTRAL, 1, 1);
    }

    public void playToggleFailSound(Entity entity) {
        entity.getLevel().playSound(null, entity.blockPosition(), toggleFailSound, SoundSource.NEUTRAL, 1, 1);
    }

    protected void placeLightBlock(Level level, BlockPos pos, int lightLevel) {
        var targetBlock = level.getBlockState(pos);
        
        if(targetBlock.isAir() || isLightBlock(targetBlock)) {
            var lightBlock = lightBlocks[lightLevel - 1];
            level.setBlock(pos, Blocks.AIR.defaultBlockState(), 144);
            level.setBlock(pos, lightBlock.defaultBlockState(), 22);
        }
    }

    protected void placeLightAround(Level level, BlockPos pos, int lightLevel) {
        placeLightBlock(level, pos, lightLevel);
        placeLightBlock(level, pos.east(), lightLevel);
        placeLightBlock(level, pos.north(), lightLevel);
        placeLightBlock(level, pos.south(), lightLevel);
        placeLightBlock(level, pos.west(), lightLevel);
    }

    protected boolean isLightBlock(BlockState bs) {
        for(var lightBlock : lightBlocks) {
            if(bs.is(lightBlock)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public void appendHoverText(ItemStack stack, Level level, List<Component> components, TooltipFlag flag) {
        if(PoweredFlashlight.commonConfig.energyDrainPerTick() > 0) {
            components.add(Component.translatable("tooltip.powered_flashlight.energy", getEnergy(stack), getCapacity(stack)));
        }
    }

    @Override
    public boolean isBarVisible(ItemStack stack) {
        return PoweredFlashlight.commonConfig.energyDrainPerTick() > 0 && getEnergy(stack) < getCapacity(stack);
    }

    @Override
    public int getBarWidth(ItemStack stack) {
        var capacity = getCapacity(stack);
        return capacity > 0 ? (int)(13 * getEnergy(stack)/(float)capacity) : super.getBarWidth(stack);
    }

    @Override
    public int getBarColor(ItemStack stack) {
        var capacity = getCapacity(stack);
        return capacity > 0 ? Mth.hsvToRgb(Math.max(0, getEnergy(stack)/(float)capacity) / 3.0F, 1.0F, 1.0F) : super.getBarColor(stack);
    }

    @Override
    public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> items) {
        if(this.allowedIn(tab)) {
            items.add(new ItemStack(this));

            if(PoweredFlashlight.commonConfig.energyDrainPerTick() > 0) {
                items.add(createPrecharged());
            }
        }
    }

    public ItemStack createPrecharged() {
        var stack = new ItemStack(this);
        stack.getOrCreateTag().putBoolean("precharged", true);
        return stack;
    }
}
