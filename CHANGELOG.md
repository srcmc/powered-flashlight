# Changelog

## [0.1.0-alpha] - 2024-01-24

***Added***

- Block: Transient light blocks (unobtainable without commands)
- Feature: Configurations
- Feature: Forge energy support
- Item: Flashlight
