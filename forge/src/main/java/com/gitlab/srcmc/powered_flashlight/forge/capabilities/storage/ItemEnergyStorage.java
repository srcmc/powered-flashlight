package com.gitlab.srcmc.powered_flashlight.forge.capabilities.storage;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.energy.EnergyStorage;

public class ItemEnergyStorage extends EnergyStorage {
    public static final String ENERGY_TAG = "energy";
    private final ItemStack stack;

    public ItemEnergyStorage(ItemStack stack, int capacity, int maxReceive, int maxExtract) {
        this(stack, capacity, maxReceive, maxExtract, 0);
    }

    public ItemEnergyStorage(ItemStack stack, int capacity, int maxReceive, int maxExtract, int energy) {
        super(capacity, maxReceive, maxExtract, energy);
        stack.getOrCreateTag().putInt(ENERGY_TAG, energy);
        this.stack = stack;
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        var extracted = super.extractEnergy(maxExtract, simulate);
        if(!simulate) stack.getOrCreateTag().putInt(ENERGY_TAG, energy);
        return extracted;
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        var received = super.receiveEnergy(maxReceive, simulate);
        if(!simulate) stack.getOrCreateTag().putInt(ENERGY_TAG, energy);
        return received;
    }
}
