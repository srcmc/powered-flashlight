/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge.client.plugins;

import com.gitlab.srcmc.powered_flashlight.items.AbstractFlashlightItem;
import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;
import com.gitlab.srcmc.powered_flashlight.forge.ModRegistries;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.ingredients.subtypes.IIngredientSubtypeInterpreter;
import mezz.jei.api.registration.ISubtypeRegistration;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

@JeiPlugin
public class JEIPugin implements IModPlugin {
    @Override
    public ResourceLocation getPluginUid() {
        return new ResourceLocation(PoweredFlashlight.MODID, "jei_plugin");
    }

    @Override
    public void registerItemSubtypes(ISubtypeRegistration registration) {
        IIngredientSubtypeInterpreter<ItemStack> chargedProvider = (stack, uid) -> {
            if(!(stack.getItem() instanceof AbstractFlashlightItem)) {
                return IIngredientSubtypeInterpreter.NONE;
            }

            if(PoweredFlashlight.commonConfig.energyDrainPerTick() > 0
            && stack.getOrCreateTag().getBoolean("precharged")) {
                return "precharged";
            }

            return IIngredientSubtypeInterpreter.NONE;
        };

        registration.registerSubtypeInterpreter(
            ModRegistries.Items.FLASHLIGHT.get(),
            chargedProvider);
    }
}