/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge.capabilities;

import com.gitlab.srcmc.powered_flashlight.forge.capabilities.storage.ItemEnergyStorage;

import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;

public class EnergyCapability implements ICapabilityProvider {
    private final LazyOptional<IEnergyStorage> energyStorage;

    public EnergyCapability(ItemStack stack, int maxEnergy, int maxReceive, int maxExtract) {
        this(stack, maxEnergy, maxReceive, maxExtract, 0);
    }

    public EnergyCapability(ItemStack stack, int maxEnergy, int maxReceive, int maxExtract, int energy) {
        var es = new ItemEnergyStorage(stack, maxEnergy, maxReceive, maxExtract, energy);
        energyStorage = LazyOptional.of(() -> es);
    }

    @Override
    public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction arg) {
        return capability == ForgeCapabilities.ENERGY ? energyStorage.cast() : LazyOptional.empty();
    }
}
