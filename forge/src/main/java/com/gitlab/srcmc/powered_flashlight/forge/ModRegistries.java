/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge;

import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;
import com.gitlab.srcmc.powered_flashlight.forge.blocks.LightBlock;
import com.gitlab.srcmc.powered_flashlight.forge.blocks.entities.LightBlockEntity;
import com.gitlab.srcmc.powered_flashlight.forge.items.FlashlightItem;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModRegistries {
    public static void init() {
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        Sounds.REGISTRY.register(bus);
        Items.REGISTRY.register(bus);
        Blocks.REGISTRY.register(bus);
        BlockEntities.REGISTRY.register(bus);
    }

    public static class Sounds {
        public static final DeferredRegister<SoundEvent> REGISTRY;
        public static final RegistryObject<SoundEvent> FLASHLIGHT_TOGGLE_ON;
        public static final RegistryObject<SoundEvent> FLASHLIGHT_TOGGLE_OFF;
        public static final RegistryObject<SoundEvent> FLASHLIGHT_TOGGLE_FAIL;

        static {
            REGISTRY = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, PoweredFlashlight.MODID);
            FLASHLIGHT_TOGGLE_ON = REGISTRY.register("item.flashlight.toggleon", () -> new SoundEvent(new ResourceLocation(PoweredFlashlight.MODID, "item.flashlight.toggleon")));
            FLASHLIGHT_TOGGLE_OFF = REGISTRY.register("item.flashlight.toggleoff", () -> new SoundEvent(new ResourceLocation(PoweredFlashlight.MODID, "item.flashlight.toggleoff")));
            FLASHLIGHT_TOGGLE_FAIL = REGISTRY.register("item.flashlight.togglefail", () -> new SoundEvent(new ResourceLocation(PoweredFlashlight.MODID, "item.flashlight.togglefail")));
        }
    }

    public static class Items {
        public static final DeferredRegister<Item> REGISTRY;
        public static final RegistryObject<FlashlightItem> FLASHLIGHT;

        static {
            REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, PoweredFlashlight.MODID);
            FLASHLIGHT = REGISTRY.register("flashlight", () -> new FlashlightItem());
        }
    }

    public static class Blocks {
        public static final DeferredRegister<Block> REGISTRY;
        public static final RegistryObject<LightBlock> LIGHT_1;
        public static final RegistryObject<LightBlock> LIGHT_2;
        public static final RegistryObject<LightBlock> LIGHT_3;
        public static final RegistryObject<LightBlock> LIGHT_4;
        public static final RegistryObject<LightBlock> LIGHT_5;
        public static final RegistryObject<LightBlock> LIGHT_6;
        public static final RegistryObject<LightBlock> LIGHT_7;
        public static final RegistryObject<LightBlock> LIGHT_8;
        public static final RegistryObject<LightBlock> LIGHT_9;
        public static final RegistryObject<LightBlock> LIGHT_10;
        public static final RegistryObject<LightBlock> LIGHT_11;
        public static final RegistryObject<LightBlock> LIGHT_12;
        public static final RegistryObject<LightBlock> LIGHT_13;
        public static final RegistryObject<LightBlock> LIGHT_14;
        public static final RegistryObject<LightBlock> LIGHT_15;

        static {
            REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, PoweredFlashlight.MODID);
            LIGHT_1 = REGISTRY.register("light_1", () -> new LightBlock(1));
            LIGHT_2 = REGISTRY.register("light_2", () -> new LightBlock(2));
            LIGHT_3 = REGISTRY.register("light_3", () -> new LightBlock(3));
            LIGHT_4 = REGISTRY.register("light_4", () -> new LightBlock(4));
            LIGHT_5 = REGISTRY.register("light_5", () -> new LightBlock(5));
            LIGHT_6 = REGISTRY.register("light_6", () -> new LightBlock(6));
            LIGHT_7 = REGISTRY.register("light_7", () -> new LightBlock(7));
            LIGHT_8 = REGISTRY.register("light_8", () -> new LightBlock(8));
            LIGHT_9 = REGISTRY.register("light_9", () -> new LightBlock(9));
            LIGHT_10 = REGISTRY.register("light_10", () -> new LightBlock(10));
            LIGHT_11 = REGISTRY.register("light_11", () -> new LightBlock(11));
            LIGHT_12 = REGISTRY.register("light_12", () -> new LightBlock(12));
            LIGHT_13 = REGISTRY.register("light_13", () -> new LightBlock(13));
            LIGHT_14 = REGISTRY.register("light_14", () -> new LightBlock(14));
            LIGHT_15 = REGISTRY.register("light_15", () -> new LightBlock(15));
        }
    }

    public static class BlockEntities {
        public static final DeferredRegister<BlockEntityType<?>> REGISTRY;
        public static final RegistryObject<BlockEntityType<LightBlockEntity>> LIGHT;

        static {
            REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, PoweredFlashlight.MODID);
            LIGHT = REGISTRY.register("light", () -> BlockEntityType.Builder.of(LightBlockEntity::new,
                Blocks.LIGHT_1.get(),
                Blocks.LIGHT_2.get(),
                Blocks.LIGHT_3.get(),
                Blocks.LIGHT_4.get(),
                Blocks.LIGHT_5.get(),
                Blocks.LIGHT_6.get(),
                Blocks.LIGHT_7.get(),
                Blocks.LIGHT_8.get(),
                Blocks.LIGHT_9.get(),
                Blocks.LIGHT_10.get(),
                Blocks.LIGHT_11.get(),
                Blocks.LIGHT_12.get(),
                Blocks.LIGHT_13.get(),
                Blocks.LIGHT_14.get(),
                Blocks.LIGHT_15.get()
            ).build(null));
        }
    }
}
