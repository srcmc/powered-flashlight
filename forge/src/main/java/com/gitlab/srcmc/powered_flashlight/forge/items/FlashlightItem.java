/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge.items;

import com.gitlab.srcmc.powered_flashlight.items.AbstractFlashlightItem;
import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;
import com.gitlab.srcmc.powered_flashlight.forge.ModForge;
import com.gitlab.srcmc.powered_flashlight.forge.ModRegistries;
import com.gitlab.srcmc.powered_flashlight.forge.capabilities.EnergyCapability;
import com.gitlab.srcmc.powered_flashlight.forge.capabilities.storage.ItemEnergyStorage;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

public class FlashlightItem extends AbstractFlashlightItem {
    public FlashlightItem() {
        super(ModForge.CreativeTab.instance, new Block[]{
            ModRegistries.Blocks.LIGHT_1.get(),
            ModRegistries.Blocks.LIGHT_2.get(),
            ModRegistries.Blocks.LIGHT_3.get(),
            ModRegistries.Blocks.LIGHT_4.get(),
            ModRegistries.Blocks.LIGHT_5.get(),
            ModRegistries.Blocks.LIGHT_6.get(),
            ModRegistries.Blocks.LIGHT_7.get(),
            ModRegistries.Blocks.LIGHT_8.get(),
            ModRegistries.Blocks.LIGHT_9.get(),
            ModRegistries.Blocks.LIGHT_10.get(),
            ModRegistries.Blocks.LIGHT_11.get(),
            ModRegistries.Blocks.LIGHT_12.get(),
            ModRegistries.Blocks.LIGHT_13.get(),
            ModRegistries.Blocks.LIGHT_14.get(),
            ModRegistries.Blocks.LIGHT_15.get()
        });

        toggleOnSound = ModRegistries.Sounds.FLASHLIGHT_TOGGLE_ON.get();
        toggleOffSound = ModRegistries.Sounds.FLASHLIGHT_TOGGLE_OFF.get();
        toggleFailSound = ModRegistries.Sounds.FLASHLIGHT_TOGGLE_FAIL.get();
    }

    // Dropping an active flashlight will sometimes not be synchronized to the client.
    // Worst case: Possible item dupe bug. Hooking into the droppedByPlayer event,
    // preventing the item from beeing dropped by its original source, and dropping it
    // explicitly here again fixes the issue. Todo: Is there a vanilla solution?
    // 
    // For more information about the issue see following post:
    // https://forums.minecraftforge.net/topic/141910-item-dupe-when-tossed-while-tag-is-beeing-updated-in-inventorytick-ghost-item/
    @Override
    public boolean onDroppedByPlayer(ItemStack stack, Player player) {
        player.getInventory().removeItem(stack);
        player.drop(stack, true);
        return false;
    }

    @Override
    public boolean onEntityItemUpdate(ItemStack stack, ItemEntity entity) {
        var level = entity.getLevel();

        if(!level.isClientSide()) {
            if(isActive(stack)) {                
                if(isPowered(stack)) {
                    placeLightAround(level, entity.blockPosition(), Math.max(1, PoweredFlashlight.commonConfig.maxLightLevel()/2));
                    placeLightAround(level, entity.blockPosition().above(), Math.max(1, PoweredFlashlight.commonConfig.maxLightLevel()/4));
                    extractEnergy(stack, PoweredFlashlight.commonConfig.energyDrainPerTick());
                } else {
                    setActive(stack, false);
                    playToggleOffSound(entity);
                }
            }
        }

        return super.onEntityItemUpdate(stack, entity);
    }

    @Override
    public void extractEnergy(ItemStack stack, int value) {
        var es = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
        if(es != null) es.extractEnergy(value, false);
    }

    @Override
    public int getEnergy(ItemStack stack) {
        var es = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
        return es != null ? es.getEnergyStored() : 0;
    }

    @Override
    public int getCapacity(ItemStack stack) {
        var es = stack.getCapability(ForgeCapabilities.ENERGY).orElse(null);
        return es != null ? es.getMaxEnergyStored() : 0;
    }

    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag nbt) {
        var preCharge = stack.getOrCreateTag().getInt(ItemEnergyStorage.ENERGY_TAG);

        if(stack.getTag().getBoolean("precharged")) {
            preCharge = PoweredFlashlight.commonConfig.energyCapacity();
            stack.removeTagKey("precharged");
        }

        return new EnergyCapability(stack,
            PoweredFlashlight.commonConfig.energyCapacity(),
            PoweredFlashlight.commonConfig.energyChargePerTick(),
            PoweredFlashlight.commonConfig.energyDrainPerTick(),
            preCharge);
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return slotChanged || isActive(oldStack) != isActive(newStack);
    }
}
