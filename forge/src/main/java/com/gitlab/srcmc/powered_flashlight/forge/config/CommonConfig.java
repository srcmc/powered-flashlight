/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge.config;

import com.gitlab.srcmc.powered_flashlight.config.ICommonConfig;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

public class CommonConfig implements ICommonConfig {
    private final ConfigValue<Integer> energyCapacityValue;
    private final ConfigValue<Integer> energyChargePerTickValue;
    private final ConfigValue<Integer> energyDrainPerTickValue;
    private final ConfigValue<Integer> lightSpacingValue;
    private final ConfigValue<Integer> lightTimeToLiveValue;
    private final ConfigValue<Integer> maxLightDistanceValue;
    private final ConfigValue<Integer> maxLightLevelValue;
    private final ConfigValue<Integer> minLightLevelValue;
    private final ConfigValue<Double> rayAngleValue;
    private final ForgeConfigSpec spec;

    public CommonConfig() {
        this(new ForgeConfigSpec.Builder());
    }

    public CommonConfig(ForgeConfigSpec.Builder builder) {
        var defaultEnergyDrainPerTick = ICommonConfig.super.energyDrainPerTick();
        var defaultEnergyChargePerTick = ICommonConfig.super.energyChargePerTick();
        var defaultEnergyCapacity = ICommonConfig.super.energyCapacity();
        var defaultLightSpacing = ICommonConfig.super.lightSpacing();
        var defaultLightTimeToLive = ICommonConfig.super.lightTimeToLive();
        var defaultMaxLightDistance = ICommonConfig.super.maxLightDistance();
        var defaultMaxLightLevel = ICommonConfig.super.maxLightLevel();
        var defaultMinLightLevel = ICommonConfig.super.minLightLevel();
        var defaultRayAngle = ICommonConfig.super.rayAngle();

        builder.push("Energy");
        energyDrainPerTickValue = builder
            .comment("# The amount of energy drained by an active flashlight per tick.",
                " If set to 0 the energy requirement for the flashlight is disabled",
                " completely and other values for energy related settings don't matter.",
                String.format("Default: %d (~ %dmin/%dfe)", defaultEnergyDrainPerTick, defaultEnergyDrainPerTick > 0 ? Math.round((defaultEnergyCapacity/(float)defaultEnergyDrainPerTick)/1200) : Integer.MAX_VALUE, defaultEnergyCapacity))
            .defineInRange("energy_drain_per_tick", defaultEnergyDrainPerTick, 0, Integer.MAX_VALUE - 1);
        energyChargePerTickValue = builder
            .comment("# The maximum rate at which a flashlight is charged per tick.",
                " Might be limited by the maximum charge rate of the charging device.",
                String.format("Default: %d (~ %dfe/%ds)", defaultEnergyChargePerTick, defaultEnergyCapacity, defaultEnergyChargePerTick > 0 ? Math.round((defaultEnergyCapacity/(float)defaultEnergyChargePerTick)/20) : Integer.MAX_VALUE))
            .defineInRange("energy_charge_per_tick", defaultEnergyChargePerTick, 0, Integer.MAX_VALUE - 1);
        energyCapacityValue = builder
            .comment("# The energy capacity of a flashlight.",
                String.format("Default: %d", defaultEnergyCapacity))
            .defineInRange("energy_capacity", defaultEnergyCapacity, 1, Integer.MAX_VALUE - 1);
        builder.pop();

        builder.push("Light");
        lightSpacingValue = builder
            .comment("# Spacing inbetween light blocks placed by lightrays.",
                String.format("Default: %d", defaultLightSpacing))
            .defineInRange("light_spacing", defaultLightSpacing, 0, Integer.MAX_VALUE - 1);
        lightTimeToLiveValue = builder
            .comment("# Number of ticks a light block stays in the world before it removes itself.",
                String.format("Default: %d", defaultLightTimeToLive))
            .defineInRange("light_time_to_live", defaultLightTimeToLive, 0, Integer.MAX_VALUE - 1);
        maxLightDistanceValue = builder
            .comment("# Max block distance of lightrays.",
                String.format("Default: %d", defaultMaxLightDistance))
            .defineInRange("max_light_distance", defaultMaxLightDistance, 0, Integer.MAX_VALUE - 1);
        maxLightLevelValue = builder
            .comment("# Maximum light level of light blocks placed by lightrays.",
                String.format("Default: %d", defaultMaxLightLevel))
            .defineInRange("max_light_level", defaultMaxLightLevel, 1, 15);
        minLightLevelValue = builder
            .comment("# Minimum light level of light blocks placed by lightrays. The further ",
                " a light block is placed from its source the lower its light level.",
                String.format("Default: %d", defaultMinLightLevel))
            .defineInRange("min_light_level", defaultMinLightLevel, 1, 15);
        rayAngleValue = builder
            .comment("# Factor to specify the angle of outgoing lightrays (up to 90°).",
                String.format("Default: %.3f", defaultRayAngle))
            .defineInRange("ray_angle", defaultRayAngle, 0D, 1D);
        builder.pop();

        spec = builder.build();
    }

    public ForgeConfigSpec getSpec() {
        return spec;
    }

    @Override
    public int energyCapacity() {
        return energyCapacityValue.get();
    }

    @Override
    public int energyChargePerTick() {
        return energyChargePerTickValue.get();
    }

    @Override
    public int energyDrainPerTick() {
        return energyDrainPerTickValue.get();
    }

    @Override
    public int lightSpacing() {
        return lightSpacingValue.get();
    }

    @Override
    public int lightTimeToLive() {
        return lightTimeToLiveValue.get();
    }

    @Override
    public int maxLightDistance() {
        return maxLightDistanceValue.get();
    }

    @Override
    public int maxLightLevel() {
        return maxLightLevelValue.get();
    }

    @Override
    public int minLightLevel() {
        return minLightLevelValue.get();
    }

    @Override
    public double rayAngle() {
        return rayAngleValue.get();
    }
}
