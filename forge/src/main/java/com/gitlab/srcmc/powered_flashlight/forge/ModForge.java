/*
 * This file is part of Powered Flashlight.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Powered Flashlight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Powered Flashlight is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Powered Flashlight. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.powered_flashlight.forge;

import com.gitlab.srcmc.powered_flashlight.PoweredFlashlight;
import com.gitlab.srcmc.powered_flashlight.forge.config.CommonConfig;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;

@Mod(PoweredFlashlight.MODID)
public class ModForge {
    static { ModRegistries.init(); }

    public ModForge() {
        var commonConfig = new CommonConfig();
        PoweredFlashlight.commonConfig = commonConfig;
        ModLoadingContext.get().registerConfig(Type.COMMON, commonConfig.getSpec());
    }

    public static class CreativeTab extends CreativeModeTab {
        public static final CreativeTab instance = new CreativeTab(CreativeModeTab.TABS.length, PoweredFlashlight.MODID);

        private CreativeTab(int index, String label) {
            super(index, label);
        }

        @Override
        public ItemStack makeIcon() {
            return ModRegistries.Items.FLASHLIGHT.get().createPrecharged();
        }
    }
}
