# Powered Flashlight

A powered flashlight item for Minecraft.

## Description

A handheld flashlight for your adventures into the darkness. Requires energy to function. This mod does not provide any means of charging a flashlight but offers support for *Forge Energy* instead, hence a flashlight can be charged with any chargers from different mods with Forge Energy support. Provides configurations to adjust the properties and behaviour of a flashlight (tip: setting `energyDrainPerTick` to `0` will disable the energy requirement completely). Works in single- and multiplayer.

## Developer Notes

|                                  |                                                  |
| -------------------------------- | ------------------------------------------------ |
| `gradlew tasks`                  | List gradle tasks.                               |
| `gradlew <modloader>:runClient`  | Run client local.                                |
| `gradlew <modloader>:runClient2` | Run client local with different username.        |
| `gradlew <modloader>:runServer`  | Run server local.                                |
| `gradlew <modloader>:build`      | Build jar archive (output directory is `build`). |
| `gradlew clean`                  | Clean workspace.                                 |

> `<modloader>` can be either `forge` or `fabric`.
